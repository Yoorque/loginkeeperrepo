//
//  NotificationName+extensions.swift
//  LoginKeeper
//
//  Created by Dusan Juranovic on 3/4/18.
//  Copyright © 2018 Dusan Juranovic. All rights reserved.
//
import UIKit

extension Notification.Name {
    static let RateAlert = Notification.Name(rawValue: "RateAlert")
}
