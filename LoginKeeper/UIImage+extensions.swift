//
//  UIImage+extensions.swift
//  LoginKeeper
//
//  Created by Dusan Juranovic on 2/11/18.
//  Copyright © 2018 Dusan Juranovic. All rights reserved.
//

import UIKit

extension UIImage {
    func resizedImage(newSize: CGSize) -> UIImage {
        guard self.size != newSize else { return self }
        let scaleFactorX = newSize.width / self.size.width
        let scaleFactorY = newSize.height / self.size.height
        
        let newWidth = self.size.width * min(scaleFactorX, scaleFactorY)
        let newHeight = self.size.height * min(scaleFactorX, scaleFactorY)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        return newImage
    }
}
